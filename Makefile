.DEFAULT_GOAL := help

help:
		@echo "-- docker"
		@echo " start                       start"
		@echo " stop                        stop"
		@echo " build                       build"
		@echo ""
		@echo "-- commands"

start:
		@docker-compose start
		@npm start --prefix albs/

stop:
		@docker-compose stop

build:
		@docker-compose pull
		@docker-compose build --no-cache
		@docker-compose create
		@docker network create internet
